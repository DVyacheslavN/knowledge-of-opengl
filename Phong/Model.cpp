#include "Model.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Model::Model(const LoadingSpecification & spec, std::shared_ptr<Shader> pShader)
    : m_pShader(pShader)
    , m_modelWorldPosition(1.0f)
{

    m_modelPrivateBase = std::make_shared<ModelPrivate>(spec);
    m_texturePrivate = std::make_shared<TexturePrivate>();
}

Model::Model(const Model &model, bool asBase)
{
    m_modelWorldPosition = model.m_modelWorldPosition;
    m_pShader = model.m_pShader;
    m_modelPrivateBase = asBase ? model.m_modelPrivateTransformed : model.m_modelPrivateBase;
    m_texturePrivate = model.m_texturePrivate;
    m_modelPrivateTransformed = nullptr;

    if (m_modelPrivateBase->m_spec.layerWorldTransformnation > -1)
    {
        updateWorldPosition(m_modelWorldPosition);
    }
}

Model::Model(const Model &model, const glm::mat4 &modelWorldPosition)
    : Model(model)
{
    m_modelWorldPosition = modelWorldPosition;
}

void Model::setModelMatrix(const std::vector<GLfloat> &modelMatrix, const std::vector<int> &ebo)
{
    m_modelPrivateBase->m_aModelMatrix = modelMatrix;
    m_modelPrivateBase->m_aEbo = ebo;
}

Shader *Model::shader()
{
    return m_pShader.get();
}

void Model::load()
{
    auto modelPrivate = m_modelPrivateTransformed == nullptr ? m_modelPrivateBase : m_modelPrivateTransformed;
    if (modelPrivate->m_bLoaded == false)
    {
        loadModelPrivate(modelPrivate);
        loadTexture(modelPrivate);

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        if (m_modelPrivateBase->m_spec.layerWorldTransformnation > -1)
        {
            updateWorldPosition(m_modelWorldPosition);
        }
        modelPrivate->m_bLoaded = true;
    }


}

void Model::use()
{
    std::shared_ptr<ModelPrivate> pModelPrivate = nullptr;
    m_pShader->use();

    if (m_modelPrivateBase->m_spec.layerWorldTransformnation < 0)
    {
        m_pShader->setMatrix("model", m_modelWorldPosition);
        pModelPrivate = m_modelPrivateBase;
        if (!pModelPrivate->m_bLoaded)
        {
            load();
        }
    }
    else {
        createTransformedModelPrivate();
        pModelPrivate = m_modelPrivateTransformed;
        if (!pModelPrivate->m_bLoaded)
        {
            load();
        }
    }

    glBindVertexArray(pModelPrivate->VAO);
    if (pModelPrivate->m_aEbo.size())
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pModelPrivate->EBO);
    }

    for(const auto &spec : pModelPrivate->m_spec.layers)
    {
        glEnableVertexAttribArray(spec.layerId);
    }

    useTexure();
}

void Model::unuse()
{
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    for(const auto &spec : m_modelPrivateBase->m_spec.layers)
    {
        glDisableVertexAttribArray(spec.layerId);
    }
}

void Model::setTexture(const std::string &texturePath
                       , const std::vector<GLfloat> &textureMap
                       , const Layer &layer
                       , const TextureSpecification &textureSpec)
{
    m_nTexture++;
    TextureData textureData(layer);

    if (layer.isValid())
    {
        //TO DO: layer cannot be set only by texture cycle, 2 cycles are required
        m_modelPrivateBase->m_spec.layers.push_back(layer);
    }
    textureData.textureMap = textureMap;

    glGenTextures(1, &textureData.TBO);
    textureData.path = texturePath;
    textureData.textureAlias = std::string("texture").append(std::to_string(m_nTexture));
    glBindTexture(GL_TEXTURE_2D, textureData.TBO);

    int width(0), height(0), nrChannels;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(texturePath.c_str(), &width, &height, &nrChannels, 0);
    if (data)
    {
        // Устанавливаем параметры наложения и фильтрации текстур (для текущего связанного объекта текстуры)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, TextureSpecValueToGlValue(textureSpec.wrapS));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, TextureSpecValueToGlValue(textureSpec.wrapT));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, TextureSpecValueToGlValue(textureSpec.minFilter));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, TextureSpecValueToGlValue(textureSpec.maxFilter));

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    stbi_image_free(data);

    m_texturePrivate->m_aTextureDatas.push_back(textureData);
}

void Model::updateWorldPosition(const glm::mat4 &matrix)
{
    m_modelWorldPosition = matrix;
    if (m_modelPrivateBase->m_spec.layerWorldTransformnation  < 0)
    {

    }
    else
    {
        createTransformedModelPrivate();
        m_modelPrivateTransformed->m_aModelMatrix = transformBuffer(m_modelPrivateBase);
        loadModelPrivate(m_modelPrivateTransformed);
        loadTexture(m_modelPrivateTransformed);

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}

glm::mat4 Model::world() const
{
    return m_modelWorldPosition;
}

void Model::draw(std::function<void (DrawType, int)> func)
{
    use();
    func(m_modelPrivateBase->m_aEbo.size() ? DrawType::eEBO
                                           : DrawType::eVAO,
         m_modelPrivateBase->m_aEbo.size() ? m_modelPrivateBase->m_aEbo.size()
                                           : m_modelPrivateBase->m_aModelMatrix.size());
    unuse();
}

int Model::TextureSpecValueToGlValue(int textureSpec)
{
    switch (textureSpec)
    {
    case TextureSpecification::LINEAR:
        return GL_LINEAR;
    case TextureSpecification::REPEAT:
        return GL_REPEAT;
    }
}

void Model::useTexure()
{
    shader()->use();
    int counter = 0;

    for(const auto &texture : m_texturePrivate->m_aTextureDatas)
    {
        glActiveTexture(GL_TEXTURE0 + counter);
        glBindTexture(GL_TEXTURE_2D, texture.TBO);
        shader()->setInt(texture.textureAlias, counter);
        counter++;
    }
}

void Model::loadTexture(std::shared_ptr<ModelPrivate> modelPrivate)
{
    glBindVertexArray(modelPrivate->VAO);
    for(auto &texture : m_texturePrivate->m_aTextureDatas)
    {
        if (!texture.layer.isValid())
        {
            continue;
        }

        glGenBuffers(1, &texture.TVBO);
        glBindBuffer(GL_ARRAY_BUFFER, texture.TVBO);
        glBufferData(GL_ARRAY_BUFFER, texture.textureMap.size() * sizeof(GLfloat), texture.textureMap.data(), GL_STATIC_DRAW);
        glVertexAttribPointer(texture.layer.layerId, texture.layer.readingSize, GL_FLOAT, GL_FALSE
                              , texture.layer.rowSize * sizeof(GLfloat)
                              , (void*)(texture.layer.offset * sizeof(GLfloat)));
    }
}

void Model::loadVOB(std::shared_ptr<Model::ModelPrivate> modelPrivate)
{
    if (modelPrivate->VBO == 0)
    {
        glGenBuffers(1, &modelPrivate->VBO);
    }

    glBindVertexArray(modelPrivate->VAO);

    glBindBuffer(GL_ARRAY_BUFFER, modelPrivate->VBO);
    glBufferData(GL_ARRAY_BUFFER, modelPrivate->m_aModelMatrix.size() * sizeof(GLfloat)
                 , modelPrivate->m_aModelMatrix.data(), GL_STATIC_DRAW);

    for(const auto &spec : modelPrivate->m_spec.layers)
    {
        glVertexAttribPointer(spec.layerId, spec.readingSize, GL_FLOAT, GL_FALSE
                              , spec.rowSize * sizeof(GLfloat)
                              , (void*)(spec.offset * sizeof(GLfloat)));
    }
}

void Model::loadModelPrivate(std::shared_ptr<Model::ModelPrivate> modelPrivate)
{
    if (modelPrivate->m_bLoaded == true)
        return;

    glGenVertexArrays(1, &modelPrivate->VAO);
    glBindVertexArray(modelPrivate->VAO);

    loadVOB(modelPrivate);

    if (modelPrivate->m_aEbo.size())
    {
        glGenBuffers(1, &modelPrivate->EBO);
        glBindVertexArray(modelPrivate->VAO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, modelPrivate->EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, modelPrivate->m_aEbo.size() * sizeof(int), modelPrivate->m_aEbo.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
}

void Model::createTransformedModelPrivate()
{
    if (m_modelPrivateTransformed == nullptr)
    {
        m_modelPrivateTransformed = std::make_shared<ModelPrivate>(m_modelPrivateBase->m_spec);
        m_modelPrivateTransformed->m_aEbo = m_modelPrivateBase->m_aEbo;
    }
}

std::vector<GLfloat> Model::transformBuffer(std::shared_ptr<ModelPrivate> modelPrivate)
{
    auto databuff = modelPrivate->m_aModelMatrix;

    auto *data = databuff.data();
    auto layer = modelPrivate->m_spec.layers.at(modelPrivate->m_spec.layerWorldTransformnation);
    for (int i = 0; i < modelPrivate->m_aModelMatrix.size(); i+=layer.rowSize)
    {
        auto iOfset = i + layer.offset;
        glm::vec4 result(data[iOfset], data[iOfset +1], data[iOfset + 2], 1);
        result = m_modelWorldPosition * result;
        data[iOfset] = result.x;
        data[iOfset + 1] = result.y;
        data[iOfset + 2] = result.z;
    }
    return databuff;
}
