#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <GL/glew.h>
//#include <glad/glad.h>
#include <GLFW/glfw3.h>

typedef glm::mat<4, 4, float, glm::defaultp> matrix;


class Camera
{
public:
    Camera(GLFWwindow * window);

    void checkCameraKeys(GLFWwindow *window);

    void mousePositionChanged(double xpos, double ypos);
    void scrolled(double xoffset, double yoffset);
    matrix viewMatrix();
    matrix projection();
    glm::vec3 position();
    ~Camera();

private:
    static void mouseCallback(GLFWwindow* window, double xpos, double ypos);
    static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

    float lastX = 400, lastY = 300;

    float yaw = -90, pitch = 0;
    float fov = 30;

    glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f);
    glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

    float deltaTime = 0.0f;	// время между текущим и последним кадрами
    float lastFrame = 0.0f; // время последнего кадра
};

#endif // CAMERA_H
