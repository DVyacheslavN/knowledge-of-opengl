#include "Camera.h"

#include <functional>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

static Camera *pCamera = nullptr;

Camera::Camera(GLFWwindow * window)
{
    pCamera = this;

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(window, Camera::mouseCallback);
    glfwSetScrollCallback(window, Camera::scrollCallback);
}

void Camera::mousePositionChanged(double xpos, double ypos)
{
        static bool firstMouse = true;
        if(firstMouse)
        {
            lastX = xpos;
            lastY = ypos;
            firstMouse = false;
        }

        float xoffset = xpos - lastX;
        float yoffset = lastY - ypos;
        lastX = xpos;
        lastY = ypos;

        float sensitivity = 0.05;
        xoffset *= sensitivity;
        yoffset *= sensitivity;

        yaw   += xoffset;
        pitch += yoffset;

        if(pitch > 89.0f)
            pitch = 89.0f;
        if(pitch < -89.0f)
            pitch = -89.0f;

        glm::vec3 direction;
        direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
        direction.y = sin(glm::radians(pitch));
        direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
        cameraFront = glm::normalize(direction);
}

void Camera::scrolled(double xoffset, double yoffset)
{
    fov -= yoffset;
  if(fov <= 1.0f)
    fov = 1.0f;
  else if(fov >= 45.0f)
      fov = 45.0f;
}

matrix Camera::viewMatrix()
{
    return glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
}

matrix Camera::projection()
{
    return glm::perspective(glm::radians(fov), 800.0f / 600.0f, 0.1f, 100.0f);
}

glm::vec3 Camera::position()
{
    return cameraPos;
}

Camera::~Camera()
{
    pCamera = nullptr;
}

void Camera::mouseCallback(GLFWwindow *window, double xpos, double ypos)
{
    if (pCamera != nullptr)
    {
        pCamera->mousePositionChanged(xpos, ypos);
    }
}

void Camera::scrollCallback(GLFWwindow *window, double xoffset, double yoffset)
{
    if (pCamera != nullptr)
    {
        pCamera->scrolled(xoffset, yoffset);
    }
}

void Camera::checkCameraKeys(GLFWwindow *window)
{
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    float cameraSpeed = 2.5f * deltaTime;

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        cameraPos += cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        cameraPos -= cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}
