#include "Renderer.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#define GLFW_DLL

using namespace glm;

using namespace std;

const int MajorVersion(3);
const int MinorVersion(3);

Renderer::Renderer()
{
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return;
    }
    glfwWindowHint(GLFW_SAMPLES, 4); // 4x кратный antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, MajorVersion); // Мы хотим использовать OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, MinorVersion);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

Renderer::~Renderer()
{
    glfwTerminate();
}

std::string Renderer::getVersion()
{
    return std::to_string(MajorVersion) + "." +  std::to_string(MinorVersion);
}

GLFWwindow *Renderer::createWindow(const string &windowName, int width, int heinght)
{
    GLFWwindow* window; // В поставляемом исходном коде, эта переменная глобальная.
    window = glfwCreateWindow(width, heinght, windowName.data(), NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        return nullptr;
    }
    return window;
}

void Renderer::framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void Renderer::processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}
