#include <iostream>

#include <GL/glew.h>
//#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cmath>


#include "LoadShader.h"
#include "Renderer.h"

#include <functional>

#include "Model.h"

//#define STB_IMAGE_IMPLEMENTATION
//#include <stb_image.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.h"
#include "Stuff.h"

glm::vec3 lightPos {1.2f, 1.0f, 2.0f};

int main()
{
    Renderer render;

    auto window1 = render.createWindow("AAAA", 1024, 768);
    glfwMakeContextCurrent(window1);

    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    glViewport(0, 0, 700, 700);
    Camera camera(window1);
    glEnable(GL_DEPTH_TEST);
    glfwSetFramebufferSizeCallback(window1, &Renderer::framebuffer_size_callback);
    glfwSetInputMode(window1, GLFW_STICKY_KEYS, GL_TRUE);

    auto programID2 = Shader( "SimpleVertexShader.vshd", "SimpleFragmentShader.fshd" );
    auto colorShader = Shader( "shaderWithLayout.vshd", "objectColorShader.fshd");
    auto lightShader = Shader( "LightingObject.vshd", "LightShader.fshd");

    Model my_model(Stuff::figure1Layers(),
                   std::shared_ptr<Shader>(&colorShader));
    my_model.setModelMatrix(Stuff::figure1cubeWithPlainVectorsForLighting());
    my_model.setTexture("/home/vyacheslav/Downloads/8_9.jpeg");

    my_model.load();
    auto models = Stuff::repeatModelbyOnAxis(my_model, 5, 5, {0, 0, 0});

    auto layers = Stuff::figure1Layers();
    layers.layerWorldTransformnation = -1;
    Model ligingObject(layers,
                       std::shared_ptr<Shader>(&lightShader));
    ligingObject.setModelMatrix(Stuff::figure1cubeWithPlainVectorsForLighting());
    do{

        glm::vec3 lightPos1 = lightPos * glm::vec3(1.0f, (float)std::sin(glfwGetTime()), (float)std::cos(glfwGetTime()));

        glClearColor(0.0f, 0.0f, 0.4f, 0.0f);           // set the color
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        my_model.shader()->use();
        my_model.shader()->setVec3("objectColor", {1.0f, 0.5f, 0.31f});
        my_model.shader()->setVec3("lightColor",  {1.0f, 1.0f, 1.0f});
        my_model.shader()->setVec3("lightPos", lightPos1);
        my_model.shader()->setVec3("viewPos", camera.position());

        my_model.shader()->setMatrix("view",  camera.viewMatrix());
        my_model.shader()->setMatrix("projection",  camera.projection());

        for (auto &modeldd : models)
        {

            modeldd.draw([&](DrawType type, int count){
                switch (type) {
                case DrawType::eEBO:
                    glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, 0);
                    break;
                case DrawType::eVAO:
                    glDrawArrays(GL_TRIANGLES, 0, count);
                    break;
                }}
            );
        }

        ligingObject.shader()->use();
        glm::mat4 model1(1.0f);

        model1 = glm::translate(model1, lightPos1);
        model1 = glm::scale(model1, glm::vec3(0.1f));


        ligingObject.updateWorldPosition(model1);
        ligingObject.shader()->setMatrix("view", camera.viewMatrix());
        ligingObject.shader()->setMatrix("projection", camera.projection());

        ligingObject.draw([&](DrawType type, int count){
            glDrawArrays(GL_TRIANGLES, 0, count);}
        );

        glfwSwapBuffers(window1);
        glfwPollEvents();

        camera.checkCameraKeys(window1);

    } // Проверяем, была ли нажата кнопка Esc или было закрыто окно?
    while(glfwWindowShouldClose(window1) == false);


    std::cout << "Hello World!" << std::endl;
    return 0;
}
