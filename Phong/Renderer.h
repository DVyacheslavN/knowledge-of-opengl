#ifndef RENDERER_H
#define RENDERER_H
#include <string>


class GLFWwindow;

class Renderer
{
public:
    Renderer();
    ~Renderer();

    std::string getVersion();
    GLFWwindow *createWindow(const std::string &windowName, int width, int heinght);

    static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
    void processInput(GLFWwindow *window);
};


#endif // RENDERER_H
