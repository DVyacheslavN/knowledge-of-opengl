#ifndef STUFF_H
#define STUFF_H

#include <vector>
#include "Model.h"

class Stuff
{
public:
    Stuff();
    static std::vector<float> figure1cubeWithPlainVectorsForLighting();
    static std::vector<float> figure2PlainWithColor();
    static std::vector<float> figure2Texture();
    static std::vector<int> figure2Elemts();
    static std::vector<float> figure3cubeWithTextureCoordinates();
    static std::vector<float> figure4PlainWithColorTexureCoordinates();
    static std::vector<int> figure4Elemts();

    static LoadingSpecification figure1Layers();
    static LoadingSpecification figure2Layers();
    static LoadingSpecification figure2TextureLayer();
    static LoadingSpecification figure3Layers();
    static LoadingSpecification figure4Layers();

    static std::vector<Model> repeatModelbyOnAxis(const Model &model, int rowCount, int columnCount, glm::vec3 axis);
};

#endif // STUFF_H
