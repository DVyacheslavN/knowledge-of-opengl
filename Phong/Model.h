#ifndef MODEL_H
#define MODEL_H

#include "LoadShader.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>
#include <tuple>
#include <list>

#include <functional>
#include <memory>

struct Layer
{
    Layer(int layerId_, int readingSize_, int rowSize_, int offset_)
        : layerId(layerId_), readingSize(readingSize_), rowSize(rowSize_), offset(offset_) {}
    Layer(){}
    int layerId = -1;
    int readingSize = -1;
    int rowSize = -1;
    int offset = 0;
    bool isValid() const {return layerId > 0 && readingSize > 0 && rowSize > 0 && offset > -1;}
};

enum DrawType
{
    eEBO = 0,
    eVAO
} typedef DrawType;

struct LoadingSpecification
{
    LoadingSpecification(std::initializer_list<Layer> list, int layerWorldTransformnation_ = -1)
        : layers(list)
        , layerWorldTransformnation(layerWorldTransformnation_) {}
    std::vector<Layer> layers;
    int layerWorldTransformnation = -1;

};

struct TextureSpecification
{
    enum SpecStates
    {
        REPEAT = 0,
        LINEAR
    };

    int wrapS = REPEAT;
    int wrapT = REPEAT;
    int minFilter = LINEAR;
    int maxFilter = LINEAR;
};


class Model
{
    struct TextureData
    {
        TextureData(Layer layer_) : layer(layer_) {}
        std::string path;
        std::string textureAlias;
        unsigned int TVAO = 0;
        unsigned int TBO = 0;
        unsigned int TVBO = 0;
        Layer layer;
        std::vector<GLfloat> textureMap;
    };

    class ModelPrivate
    {
    public:
        ModelPrivate(const LoadingSpecification & spec) : m_spec(spec) {}
        std::vector<GLfloat> m_aModelMatrix;
        std::vector<int> m_aEbo;
        LoadingSpecification m_spec;
        unsigned int VAO = 0, VBO = 0, EBO = 0;
        bool m_bLoaded = false;
    };

    class TexturePrivate
    {
    public:
        std::list<TextureData> m_aTextureDatas;
    };

public:
    Model(const LoadingSpecification & spec, std::shared_ptr<Shader>pShader = nullptr);
    Model(const Model &model, const glm::mat4 &modelWorldPosition);
    Model(const Model &model, bool asBase = false);

    void setModelMatrix(const std::vector<GLfloat> &modelMatrix, const std::vector<int> &ebo = {});
    Shader *shader();
    void use();
    void unuse();
    void load();
    void setTexture(const std::string &texturePath
                    , const std::vector<GLfloat> &textureMap = {}
                     , const Layer &layer = Layer(), const TextureSpecification &textureSpec = TextureSpecification());
    void updateWorldPosition(const glm::mat4 &matrix);
    glm::mat4 world() const;
    //!
    //! \brief draw
    //! int is amount of vertexs
    void draw(std::function<void(DrawType, int)> func);

private:
    int TextureSpecValueToGlValue(int textureSpec);
    void useTexure();
    void loadTexture(std::shared_ptr<Model::ModelPrivate> modelPrivate);
    void loadVOB(std::shared_ptr<ModelPrivate> modelPrivate);
    void loadModelPrivate(std::shared_ptr<ModelPrivate> modelPrivate);
    void createTransformedModelPrivate();

    std::vector<GLfloat> transformBuffer(std::shared_ptr<ModelPrivate> modelPrivate);

    std::shared_ptr<Shader>m_pShader = nullptr;
    std::shared_ptr<ModelPrivate> m_modelPrivateBase = nullptr;
    std::shared_ptr<ModelPrivate> m_modelPrivateTransformed = nullptr;
    std::shared_ptr<TexturePrivate> m_texturePrivate = nullptr;

    glm::mat4 m_modelWorldPosition;
    unsigned int MBO;

    int m_nTexture = 0;
};

#endif // MODEL_H
